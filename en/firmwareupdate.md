---
title: Current Release
---
# Current: Mushroom (2019-12-09)
- Firmware Download: https://card10.badge.events.ccc.de/release/card10-v1.13-Mushroom.zip
- [Changelog](https://git.card10.badge.events.ccc.de/card10/firmware/blob/master/CHANGELOG.md#v113-2019-12-09-mushroom)

# Update the firmware release on your card10
* download the current firmware `.zip` file to your computer
* extract it
* put your card10 into [USB storage mode](/en/gettingstarted#usb-storage)
* copy over the files you just extracted directly into the main folder. Note: on macOS devices it is recommended to use the terminal, e.g. for the broccoli release: `cp -r ~/Downloads/card10-v1.2-broccoli/* /Volumes/CARD10/`
* eject your device (if you're doing this in the command line: don't forget the `sync` on linux)
* switch your card10 off and on again

---

# Previous Releases
### Leek (2019-10-19)
- Firmware Download: https://card10.badge.events.ccc.de/release/card10-v1.12-Leek.zip
- [Changelog](https://git.card10.badge.events.ccc.de/card10/firmware/blob/master/CHANGELOG.md#v112-2019-10-19-leek)

### Karotte (2019-09-24)
- Firmware Download: https://card10.badge.events.ccc.de/release/card10-v1.11-Karotte.zip
- [Changelog](https://git.card10.badge.events.ccc.de/card10/firmware/blob/master/CHANGELOG.md#v111-2019-09-24-karotte)

### JerusalemArtichoke (2019-09-05 21:42)
- Firmware Download: https://card10.badge.events.ccc.de/release/card10-v1.10-JerusalemArtichoke.zip
- [Changelog](https://git.card10.badge.events.ccc.de/card10/firmware/blob/master/CHANGELOG.md#v110-2019-09-05-2142-jerusalemartichoke)

### IcebergLettuce (2019-08-28 23:23)
- Firmware Download: https://card10.badge.events.ccc.de/release/card10-v1.9-IcebergLettuce.zip
- **Jailbreak relase is no longer necessary! You need to adjust card10.cfg now.**
- [Changelog](https://git.card10.badge.events.ccc.de/card10/firmware/blob/master/CHANGELOG.md#v19-2019-08-28-2323-iceberglettuce)

### HabaneroChilli (2019-08-27 - 11:38)
- Firmware Download: https://card10.badge.events.ccc.de/release/card10-v1.8-HabaneroChilli.zip
- Firmware Download (with jailbreak): https://card10.badge.events.ccc.de/release/card10-v1.8-HabaneroChilli-jailbreak.zip
- [Changelog](https://git.card10.badge.events.ccc.de/card10/firmware/blob/master/CHANGELOG.md#v18-2019-08-27-1138-habanerochilli)

### Garlic (day 4 - 21:48)
- Firmware Download: https://card10.badge.events.ccc.de/release/card10-v1.7-Garlic.zip
- Firmware Download (with jailbreak): https://card10.badge.events.ccc.de/release/card10-v1.7-Garlic-jailbreak.zip
- [Changelog](https://git.card10.badge.events.ccc.de/card10/firmware/blob/master/CHANGELOG.md#v17-2019-08-24-2148-garlic)

### Fennel (day 3 - 20:30)
- Firmware Download: https://card10.badge.events.ccc.de/release/card10-v1.6-Fennel.zip
- [Changelog](https://git.card10.badge.events.ccc.de/card10/firmware/blob/master/CHANGELOG.md#v16-2019-08-23-2030-fennel)

### Eggppppplant (day 3 - 00:23)
- Firmware Download: https://card10.badge.events.ccc.de/release/card10-v1.5-Eggppppplant.zip
- [Changelog](https://git.card10.badge.events.ccc.de/card10/firmware/blob/master/CHANGELOG.md#v15-2019-08-23-0018-eggppppplant)

### DaikonRadish (day 2 - 20:00)
- Firmware Download: https://card10.badge.events.ccc.de/release/card10-v1.4-DaikonRadish.zip
- [Changelog](https://git.card10.badge.events.ccc.de/card10/firmware/blob/master/CHANGELOG.md#v14-2019-08-22-1943-daikonradish)

### CCCauliflower (day 2 - 00:30)
- Firmware Download: https://card10.badge.events.ccc.de/release/card10-v1.3-cccauliflower.zip
- [Changelog](https://git.card10.badge.events.ccc.de/card10/firmware/blob/master/CHANGELOG.md#v13-2019-08-22-0012-cccauliflower)

### Broccoli (day 1 - 19:00)
- Firmware Download: https://card10.badge.events.ccc.de/release/card10-v1.2-broccoli.zip

### Asparagus
As of Day 1, 18:00
Asparagus is the firmware that is loaded on the card10s handed out on day 1 from 18:00

---

There are currently no previous releases
