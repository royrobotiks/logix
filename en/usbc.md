---
title: USB-C
---

# General use

You will need a USB-C cable to charge your card10. You can also use it as a storage device to read/write files and update software. We plan to provide most of these functions via BLE as well, but a wired connection might still come handy.
The charging and file transfer works whichever way round the USB-C cable is plugged in.

# Full I/O

Traveler reports agree that the badge uses USB-C, the assumption is that this was chosen over Micro-USB for the available range of extra pins. The reconstruction thus has some extra pins available, which would explain several observations of hardware interHacktions using USB-C.

| Pin Name | Pin Number | Usage               | Comment                                               |
| ---------|------------|---------------------|-------------------------------------------------------|
| SBU1     | A8         | SWDIO (GPIO)        | Primary use for SWD. Firmware can reconfigure to GPIO |
| SBU2     | B8         | SWCLK (GPIO)        | Primary use for SWD. Firmware can reconfigure to GPIO |
| RX1+     | B11        | ECG P               | ECG P can be switched between this pin and the wristband via firmware |
| RX1-     | B10        | ECG N               |                                                       |
| TX1+     | A2         | ECG COM             | ECG common mode buffer output.                        |
| TX1-     | A3         | -                   |                                                       |
| RX2+     | A11        | UART RX             |                                                       |
| RX2-     | A10        | UART TX (via Diode) | The diode protects USB tranceivers                    |
| TX2+     | B2         | Reset               | Pull low to trigger a reset                           |
| TX2-     | B3         | -                   |                                                       |

# Schematics
![USB-C related schematics section](/media/hardware/usb-c-pinout.png)

**!Caution!** Most USB-C cables available for sale do not connect all pins of the connector. Whilst this is not a problem for general use, if you want to access the full available I/O contacts, it might be best to choose a cable from the suggestions listed at the bottom of the page.

To use full I/O, there is a 'correct side up' for the USB-C cable.

## SWD
## UART
## ECG
External ECG electrodes can be connected via USB-C with a [self-made cable](/en/ecg_kit_assembly/).
## GPIOs

## 'Confirmed' full pin cable options
Whilst this list does not guarantee you anything, these cables have been bought before and were found to have all contacts connected:
 - Anker AK-A8183011
